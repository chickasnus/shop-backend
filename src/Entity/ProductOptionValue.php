<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductOptionValueRepository")
 */
class ProductOptionValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductVariant", mappedBy="productOptionValues")
     */
    private $productVariant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductOptions", inversedBy="productOptionValues")
     */
    private $productOptions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SaleProduct", mappedBy="productOptionValue")
     */
    private $saleProducts;

    public function __construct()
    {
        
        $this->productVariant = new ArrayCollection();
        $this->saleProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }


    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariant(): Collection
    {
        return $this->productVariant;
    }

    public function addProductVariant(ProductVariant $productVariant): self
    {
        if (!$this->productVariant->contains($productVariant)) {
            $this->productVariant[] = $productVariant;
        }

        return $this;
    }

    public function removeProductVariant(ProductVariant $productVariant): self
    {
        if ($this->productVariant->contains($productVariant)) {
            $this->productVariant->removeElement($productVariant);
        }

        return $this;
    }

    public function getProductOptions(): ?ProductOptions
    {
        return $this->productOptions;
    }

    public function setProductOptions(?ProductOptions $productOptions): self
    {
        $this->productOptions = $productOptions;

        return $this;
    }

    public function __toString(){
        return $this->value;
    }

    /**
     * @return Collection|SaleProduct[]
     */
    public function getSaleProducts(): Collection
    {
        return $this->saleProducts;
    }

    public function addSaleProduct(SaleProduct $saleProduct): self
    {
        if (!$this->saleProducts->contains($saleProduct)) {
            $this->saleProducts[] = $saleProduct;
            $saleProduct->addProductOptionValue($this);
        }

        return $this;
    }

    public function removeSaleProduct(SaleProduct $saleProduct): self
    {
        if ($this->saleProducts->contains($saleProduct)) {
            $this->saleProducts->removeElement($saleProduct);
            $saleProduct->removeProductOptionValue($this);
        }

        return $this;
    }

}
