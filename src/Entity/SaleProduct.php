<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SaleProduct
 *
 * @ORM\Table(name="sale_product")
 * @ORM\Entity(repositoryClass="App\Repository\SaleProductRepository")
 */
class SaleProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="saleProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sale", inversedBy="saleProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sale;


    /**
     * @var int|null
     *
     * @ORM\Column(name="quantity_product", type="integer", nullable=true)
     */
    private $quantityProduct;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductOptionValue", inversedBy="saleProducts")
     */
    private $productOptionValue;

    /**
     * @ORM\Column(type="float")
     */
    private $unitPrice;

    public function __construct()
    {
        $this->productOptionValue = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantityProduct(): ?int
    {
        return $this->quantityProduct;
    }

    public function setQuantityProduct(?int $quantityProduct): self
    {
        $this->quantityProduct = $quantityProduct;

        return $this;
    }


    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getSale(): ?Sale
    {
        return $this->sale;
    }

    public function setSale(?Sale $sale): self
    {
        $this->sale = $sale;

        return $this;
    }


    /**
     * @return Collection|ProductOptionValue[]
     */
    public function getProductOptionValue(): Collection
    {
        return $this->productOptionValue;
    }

    public function addProductOptionValue(ProductOptionValue $productOptionValue): self
    {
        if (!$this->productOptionValue->contains($productOptionValue)) {
            $this->productOptionValue[] = $productOptionValue;
        }

        return $this;
    }

    public function removeProductOptionValue(ProductOptionValue $productOptionValue): self
    {
        if ($this->productOptionValue->contains($productOptionValue)) {
            $this->productOptionValue->removeElement($productOptionValue);
        }

        return $this;
    }

    public function getUnitPrice(): ?float
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(float $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

}
