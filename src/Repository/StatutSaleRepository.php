<?php

namespace App\Repository;

use App\Entity\StatutSale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatutSale|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutSale|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutSale[]    findAll()
 * @method StatutSale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutSaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutSale::class);
    }

    // /**
    //  * @return StatutSale[] Returns an array of StatutSale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatutSale
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
