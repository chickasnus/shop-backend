<?php

namespace App\Repository;

use App\Entity\StatusSale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatusSale|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusSale|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusSale[]    findAll()
 * @method StatusSale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusSaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusSale::class);
    }

    // /**
    //  * @return StatusSale[] Returns an array of StatusSale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatusSale
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
