<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Entity\ProductVariant;
use App\Form\ProductVariantType;
use App\Repository\ProductRepository;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_home")
     */
    public function index()
    {
        $title = 'Home';

        return $this->render('admin/index.html.twig', [
            'title' => $title,
        ]);
    }

    /**
     * @Route("/admin/customers", name="admin_customers")
     */
    public function customers(CustomerRepository $customerRepo, PaginatorInterface $paginator, Request $request)
    {
        $customers = $customerRepo->findCustomersByOrderLastName();
        
        $pagination = $paginator->paginate(
            $customers, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );
        
        return $this->render('admin/customers.html.twig', [
            'customers' => $pagination,
        ]);
    }

    /**
     * @Route("/admin/customer/{id}", name="admin_customers_details")
     */
    public function customersDetails($id,CustomerRepository $customerRepo)
    {
        $customer = $customerRepo->find($id);
        $title = 'Customer Details';

        return $this->render('admin/customerdetails.html.twig', [
            'title' => $title,
            'customer' => $customer
        ]);
    }

    /**
     * @Route("/admin/sales", name="admin_sales")
     */
    public function sales()
    {
        
        $title = 'Sales';

        return $this->render('admin/sales.html.twig', [
            'title' => $title,
        ]);
    }

    /**
     * @Route("/admin/products", name="admin_products")
     */
    public function listProducts(ProductRepository $productRepo) 
    {
        $products = $productRepo->findAll();
        
        return $this->render('/admin/products.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     *  @Route("/admin/product/add", name="admin_product_add")
     */
    public function addProduct(Request $request, EntityManagerInterface $em)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form -> handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->get('productOptionValues')->getData();
            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('admin_customers');
        }
        return $this->render("/admin/product_form.html.twig", [
            "form"=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/product-variant/add", name="admin_product_variant_add")
     */
    public function addProductVariant(Request $request, EntityManagerInterface $em)
    {
        $variant = new ProductVariant();
        $form = $this->createForm(ProductVariantType::class, $variant);
        $form -> handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData());
            //$variant = $form->getData();
            //dd($form->get('price')->getData());
             $variant->setProduct($form->get('product')->getData())
                 ->setPrice($form->get('price')->getData())
                 ;
             foreach($form->get('productOptionValues')->getData() as $productOtionValue){
                     $variant->addProductOptionValue($productOtionValue);
                 }
                
                

            $em->persist($variant);
            $em->flush();
            return $this->redirectToRoute('admin_customers');
        }
        return $this->render("/admin/product_form.html.twig", [
            "form"=>$form->createView()
        ]);
    }
}
