<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AddUserCommand extends Command
{
    protected static $defaultName = 'add:user';

    private $em;
    private $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        
        $this->em = $em;
        $this->encoder = $encoder;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a user and his password')
            ->addArgument('email', InputArgument::OPTIONAL, 'What is the email of user?');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $user = new User();
        if ($email) {      
            $user->setEmail($email);
            //$user->setPassword($this->encoder->encodePassword($user, $password));
        }else{
            $email = $io->ask('Enter a email User?');
            $user->setEmail($email);
        }
        $password = $io->askHidden("Enter a password for the user: $email ?");
        $user->setPassword($this->encoder->encodePassword($user, $password));
            $this->em->persist($user);
            $this->em->flush();
        
        

        $io->success("Success add User: $email");

        return 0;
    }
}
