<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200502232157 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_option_value (id INT AUTO_INCREMENT NOT NULL, product_options_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_A938C737A9BC5ADA (product_options_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_variant (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, price VARCHAR(255) DEFAULT NULL, INDEX IDX_209AA41D4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_variant_product_option_value (product_variant_id INT NOT NULL, product_option_value_id INT NOT NULL, INDEX IDX_612C6EDDA80EF684 (product_variant_id), INDEX IDX_612C6EDDEBDCCF9B (product_option_value_id), PRIMARY KEY(product_variant_id, product_option_value_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale_product_product_option_value (sale_product_id INT NOT NULL, product_option_value_id INT NOT NULL, INDEX IDX_1499B403B8D0821E (sale_product_id), INDEX IDX_1499B403EBDCCF9B (product_option_value_id), PRIMARY KEY(sale_product_id, product_option_value_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_options (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        
        

        $this->addSql('ALTER TABLE product_option_value ADD CONSTRAINT FK_A938C737A9BC5ADA FOREIGN KEY (product_options_id) REFERENCES product_options (id)');
        $this->addSql('ALTER TABLE product_variant ADD CONSTRAINT FK_209AA41D4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_variant_product_option_value ADD CONSTRAINT FK_612C6EDDA80EF684 FOREIGN KEY (product_variant_id) REFERENCES product_variant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_variant_product_option_value ADD CONSTRAINT FK_612C6EDDEBDCCF9B FOREIGN KEY (product_option_value_id) REFERENCES product_option_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sale_product_product_option_value ADD CONSTRAINT FK_1499B403B8D0821E FOREIGN KEY (sale_product_id) REFERENCES sale_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sale_product_product_option_value ADD CONSTRAINT FK_1499B403EBDCCF9B FOREIGN KEY (product_option_value_id) REFERENCES product_option_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sale_product ADD unit_price DOUBLE PRECISION NOT NULL/*, DROP product_variant_id*/');
        $this->addSql('INSERT INTO product_options VALUE(1,"color")');
        $this->addSql('INSERT INTO product_option_value (value) SELECT color FROM sale_product GROUP BY color');
        $this->addSql('UPDATE product_option_value SET product_options_id = 1');

        $this->addSql("INSERT INTO product_variant VALUE(1,1,51.86)");
        $this->addSql("INSERT INTO product_variant VALUE(2,2,94.86)");
        $this->addSql("INSERT INTO product_variant VALUE(3,3,62.88)");
        $this->addSql("INSERT INTO product_variant VALUE(4,4,58.81)");
        $this->addSql("INSERT INTO product_variant VALUE(5,5,99.93)");
        $this->addSql("INSERT INTO product_variant VALUE(6,6,16.84)");
        $this->addSql("INSERT INTO product_variant VALUE(7,7,50.33)");
        $this->addSql("INSERT INTO product_variant VALUE(8,8,22.84)");
        $this->addSql("INSERT INTO product_variant VALUE(9,9,77.98)");
        $this->addSql("INSERT INTO product_variant VALUE(10,10,73.71)");
        for ($prod=0; $prod < 10; $prod++) { 
            for ($i=0; $i < 15; $i++) { 
                $this->addSql("INSERT INTO product_variant_product_option_value VALUE($prod+1,$i+1)");
            }  
        }
        $this->addSql('UPDATE sale_product sp SET unit_price = (SELECT price FROM product p WHERE p.id = sp.product_id )');
        $this->addSql('INSERT INTO sale_product_product_option_value (sale_product_id , product_option_value_id ) SELECT sp.id,pov.id FROM product_option_value pov INNER JOIN sale_product sp ON pov.value = sp.color');
        $this->addSql('ALTER TABLE sale_product DROP color');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_variant_product_option_value DROP FOREIGN KEY FK_612C6EDDEBDCCF9B');
        $this->addSql('ALTER TABLE sale_product_product_option_value DROP FOREIGN KEY FK_1499B403EBDCCF9B');
        $this->addSql('ALTER TABLE product_variant_product_option_value DROP FOREIGN KEY FK_612C6EDDA80EF684');
        $this->addSql('ALTER TABLE product_option_value DROP FOREIGN KEY FK_A938C737A9BC5ADA');
        $this->addSql('DROP TABLE product_option_value');
        $this->addSql('DROP TABLE product_variant');
        $this->addSql('DROP TABLE product_variant_product_option_value');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE sale_product_product_option_value');
        $this->addSql('DROP TABLE product_options');
        $this->addSql('ALTER TABLE sale_product ADD product_variant_id INT DEFAULT NULL, DROP unit_price');
    }
}
