<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425162620 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sale ADD update_statut DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql("INSERT INTO statut_sale VALUE (1,'saved')");
        $this->addSql("INSERT INTO statut_sale VALUE (2,'prepare')");
        $this->addSql("INSERT INTO statut_sale VALUE (3,'ready')");
        $this->addSql("INSERT INTO statut_sale VALUE (4,'shipped')");
        
        $this->addSql("UPDATE sale SET statut_id = 4");
        
        $this->addSql("UPDATE sale SET update_statut = date_of_purchase");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sale DROP update_statut');
    }
}
